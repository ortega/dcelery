# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import api_lenta, api_lenta2,api_lenta3, task_status,UserChatView
from django.views.generic import TemplateView
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name="home.html"), name="home"),
    url(r'^api-lenta/$', api_lenta, name="api_lenta"),
    url(r'^api-lenta2/$', api_lenta2, name="api_lenta2"),
    url(r'^api-lenta3/$', api_lenta3, name="api_lenta3"),
    url(r'^userchat/$', UserChatView.as_view(), name='user_chat'),
    url(r'^task/(?P<task_id>[\w\d\-.]+)/status/$', task_status),
]
