from django.shortcuts import render
from django.http import JsonResponse

from celery import states
from celery.utils import get_full_cls_name
from celery.utils.encoding import safe_repr
from celery.result import AsyncResult

'''
Test para probar

# detener cAhkinWorker en local
sudo supervisorctl stop cAhkinWorker
workon dcelery
cd /PATH/sitios/dcelery


/PATH/entornos/dcelery/bin/celery worker -A dcelery --loglevel=INFO

# Probar que funcione celery
python manage.py shell
from api.tasks import *

add.delay(2,2)

http://aptuz.com/blog/implementing-websockets-with-django/

'''

# Create your views here.
def api_lenta(request):
    from .tasks import api_lenta_task
    # mando a procesar la tarea y obtengo el ID
    task = api_lenta_task.delay()
    return render(request, 'api_lenta.html', {'task_id': task.id})


def api_lenta2(request):
    from .tasks import api_lenta_task2
    # mando a procesar la tarea y obtengo el ID

    if not request.session.session_key:
        request.session.save()
    task = api_lenta_task2.delay(session_key=request.session.session_key)
    return render(request, 'api_lenta2.html', {"session_key": request.session.session_key})


def api_lenta3(request):
    from .tasks import api_lenta_task3
    # mando a procesar la tarea y obtengo el ID


    task = api_lenta_task3.delay(facility="api-lenta3")
    return render(request, 'api_lenta3.html', )



def task_status(request, task_id):

    result = AsyncResult(task_id)
    # state, retval = result.state, result.result
    try:
        state = result.state
    except:
        state = 'FAILURE'

    response_data = {'id': task_id, 'status': state, 'result': ""}

    if state in states.EXCEPTION_STATES:
        # traceback = result.traceback
        try:
            resultado_error = safe_repr(result.result)
        except AttributeError as error_celery:
            resultado_error = str(error_celery)

        response_data.update({'result': resultado_error})
    else:  # Si esta en PROCESS, STARTED, SUCCESS..
        response_data.update({'result': result.result})

    return JsonResponse({'task': response_data})


from django.contrib.auth.models import User, Group
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.views.decorators.csrf import csrf_exempt
from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher

class UserChatView(TemplateView):
    template_name = 'user_chat.html'

    def get_context_data(self, **kwargs):
        context = super(UserChatView, self).get_context_data(**kwargs)
        context.update(users=User.objects.all())
        return context

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(UserChatView, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        redis_publisher = RedisPublisher(facility='foobar', users=[request.POST.get('user')])
        message = RedisMessage(request.POST.get('message'))
        redis_publisher.publish_message(message)
        return HttpResponse('OK')


'''
from ws4redis.redis_store import RedisMessage
from ws4redis.publisher import RedisPublisher

redis_publisher = RedisPublisher(facility='foobar', users=["admin2"])
message = RedisMessage("hola")
redis_publisher.publish_message(message)


'''