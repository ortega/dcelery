# Create your tasks here
from celery import shared_task, current_task
from dcelery.celery import app
import datetime
import time
import json
from ws4redis.publisher import RedisPublisher
from ws4redis.redis_store import RedisMessage


@shared_task
def add(x, y):
    return x + y


@shared_task(queue="trafico", routing_key="trafico")
def resta(x, y):
    return x - y


@shared_task(queue="trafico", routing_key='trafico')
def mul(x, y):
    return x * y


@shared_task(queue="trafico")
def suma_dias():
    return datetime.datetime.now() + datetime.timedelta(days=2)


@shared_task
def api_lenta_task():
    # Actualizamos el status del tasks con un mensaje para que el tenga una idea del avance
    current_task.update_state(state='PROGRESS', meta={'mensaje': "Filtrando datos"})
    # Simulamos que tarda 3 seg
    time.sleep(3)

    current_task.update_state(state='PROGRESS', meta={'mensaje': "Procesando datos"})
    # Simulamos que tarda 4 en procesar la informacion
    time.sleep(4)

    result_dic = {
        "cliente": "pedro",
        "consumo": "40GB"
    }
    resultado = json.dumps(result_dic)
    return resultado
@shared_task
def api_lenta_task2_back():
    # Actualizamos el status del tasks con un mensaje para que el tenga una idea del avance
    current_task.update_state(state='PROGRESS', meta={'mensaje': "Filtrando datos"})
    # Simulamos que tarda 3 seg
    time.sleep(3)

    current_task.update_state(state='PROGRESS', meta={'mensaje': "Procesando datos"})
    # Simulamos que tarda 4 en procesar la informacion
    time.sleep(4)

    result_dic = {
        "cliente": "pedro",
        "consumo": "40GB"
    }
    resultado = json.dumps(result_dic)
    return resultado

@shared_task(bind=True)
def api_lenta_task2(self, session_key):
    redis_publisher = RedisPublisher(
        facility=session_key,
        broadcast=True
    )
    # Actualizamos el status del tasks con un mensaje para que el tenga una idea del avance
    meta = {'mensaje': "Filtrando datos..", "status": "PROGRESS"}
    #current_task.update_state(state='PROGRESS', meta=meta)
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))

    # Simulamos que tarda 3 seg
    time.sleep(3)

    meta = {'mensaje': "Procesando datos..", "status": "PROGRESS"}
    #current_task.update_state(state='PROGRESS', meta=meta)
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))
    # Simulamos que tarda 4 en procesar la informacion
    time.sleep(4)

    result_dic = {
        "cliente": "pedro",
        "consumo": "40GB"
    }
    resultado = json.dumps(result_dic)

    meta = {'mensaje': resultado, "status": "DONE"}
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))
    return resultado


@shared_task(bind=True)
def api_lenta_task3(self, facility):
    # redis_publisher = RedisPublisher(facility=facility,broadcast=True )
    redis_publisher = RedisPublisher(facility="facility", users=['admin2',] )
    # Actualizamos el status del tasks con un mensaje para que el tenga una idea del avance
    meta = {'mensaje': "Filtrando datos3..", "status": "PROGRESS"}
    #current_task.update_state(state='PROGRESS', meta=meta)
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))

    # Simulamos que tarda 3 seg
    time.sleep(3)

    meta = {'mensaje': "Procesando datos3..", "status": "PROGRESS"}
    #current_task.update_state(state='PROGRESS', meta=meta)
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))
    # Simulamos que tarda 4 en procesar la informacion
    time.sleep(4)

    result_dic = {
        "cliente": "pedro",
        "consumo": "40GB"
    }
    resultado = json.dumps(result_dic)

    meta = {'mensaje': resultado, "status": "DONE"}
    meta.update({"task_id": self.request.id})
    redis_publisher.publish_message(RedisMessage(
        json.dumps(meta)
    ))
    return resultado
